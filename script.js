const form = document.getElementById('form');
const usernameInput = document.getElementById('username');
const emailInput = document.getElementById('email');
const password1Input = document.getElementById('password1');
const password2Input = document.getElementById('password2');
const button = document.getElementById('button')
const error1 = document.getElementById('error1')
const error2 = document.getElementById('error2')
const error3 = document.getElementById('error3')
const error4 = document.getElementById('error4')

form.addEventListener('submit', (event) => {
    event.preventDefault()

    const username = usernameInput.value
    const email = emailInput.value
    const password1 = password1Input.value
    const password2 = password2Input.value
    const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*])(?!.*\s).{6,20}$/;

    if (!username) {
        error1.innerHTML = "Enter your username"
        error1.classList.remove("green")
        error1.classList.add("red")
    }
    else {
        error1.innerHTML = "Valid username"
        error1.classList.remove("red")
        error1.classList.add("green")
    }
    // Below code for check email feild blank or not.
    if (!email) {
        error2.innerHTML = "Enter valid email address"
        error2.classList.remove("green")
        error2.classList.add("red")
    }

    // Below code for check email validation.
    const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if (emailInput.value.match(validRegex)) {
        error2.innerHTML = "Valid email"
        error2.classList.remove("red")
        error2.classList.add("green")
    }
    else {
        error2.innerHTML = "Enter valid email address"
        error2.classList.remove("green")
        error2.classList.add("red")
    }

    if (!password1) {
        error3.innerHTML = "Enter your password"
        error3.classList.remove("green")
        error3.classList.add("red")
    }
    // Below code for check password minimum 6 Character
    else if (!password1.match(passwordRegex)) {
        error3.innerHTML = "Required:6-20 length 1 digit, lowercase, uppercase, (!@#$%^&*)."
        error3.classList.remove("green")
        error3.classList.add("red")
    }
    // below code for maximum 20 characters and a number and special character.
    else {
        error3.innerHTML = "Valid password"
        error3.classList.remove("red")
        error3.classList.add("green")
    }

    if (!password2) {
        error4.innerHTML = "Confirm your password";
        error4.classList.remove("green")
        error4.classList.add("red");
    } else if (password1 !== password2) {
        error4.innerHTML = "Passwords do not match!";
        error4.classList.remove("green")
        error4.classList.add("red");
    } else {
        error4.innerHTML = "Passwords matched!";
        error4.classList.remove("red")
        error4.classList.add("green");
    }

})





